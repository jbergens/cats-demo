# README #

This is a manually made clone of https://github.com/architect-examples/arc-example-working-locally

### What is this repository for? ###

* It is used to experiment with Architect and AWS.

### How do I get set up? ###

* Install arc globally. See [https://arc.codes/](https://arc.codes/)
* Run `npm install`
* Try `arc sandbox` or `arc test`
* Configure your own AWS access keys and similar and then try `arc deploy --name your-name`

### Contribution guidelines ###

Currently not really open to contributions.

### Who do I talk to? ###

* jbergens
